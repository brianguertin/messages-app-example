package com.brianguertin.example.messages.data

class Attachment(val id: String,
                 val title: String,
                 val thumbnailUrl: String)