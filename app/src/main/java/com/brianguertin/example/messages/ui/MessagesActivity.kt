package com.brianguertin.example.messages.ui

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.brianguertin.example.messages.R
import com.brianguertin.example.messages.databinding.MessageListBinding
import com.brianguertin.example.messages.data.Attachment
import com.brianguertin.example.messages.data.Message
import com.brianguertin.example.messages.ui.ViewModelFactory.Companion.getViewModelInstance

class MessagesActivity : AppCompatActivity(), MessageListener {

    private lateinit var binding: MessageListBinding
    private lateinit var viewModel: MessagesViewModel

    private var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.message_list)
        viewModel = getViewModelInstance()
        setupList()
    }

    private fun setupList() {
        val adapter = MessagesAdapter(this)
        val layoutManager = LinearLayoutManager(this)
        binding.list.layoutManager = layoutManager
        binding.list.adapter = adapter
        viewModel.messages.observe(this, Observer {
            adapter.setItems(it ?: listOf())
        })
        binding.list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                viewModel.onScroll(layoutManager.findLastVisibleItemPosition())
            }
        })
    }

    override fun onLongClick(message: Message) {
        showDeleteConfirmation(R.string.delete_message_title, {
            viewModel.onConfirmDeleteMessage(message)
        })
    }

    override fun onLongClick(attachment: Attachment) {
        showDeleteConfirmation(R.string.delete_attachment_title, {
            viewModel.onConfirmDeleteAttachment(attachment)
        })
    }

    private fun showDeleteConfirmation(
            @StringRes titleRes: Int,
            onConfirm: () -> Unit) {
        AlertDialog.Builder(this@MessagesActivity)
                .setTitle(titleRes)
                .setOnDismissListener { dialog = null }
                .setPositiveButton(R.string.delete, { _, _ ->
                    onConfirm()
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog?.dismiss()
        viewModel.onDestroy()
    }
}
