package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface MessageDao {
    @Insert
    fun insert(repo: MessageEntity)

    @Query("DELETE FROM messages WHERE id = :messageId")
    fun delete(messageId: Long)

    @Query("SELECT * FROM messages WHERE id > :afterMessageId LIMIT :limit")
    fun list(afterMessageId: Long, limit: Int): List<MessageEntity>
}
