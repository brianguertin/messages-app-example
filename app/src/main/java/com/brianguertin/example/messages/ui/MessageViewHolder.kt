package com.brianguertin.example.messages.ui

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.brianguertin.example.messages.R
import com.brianguertin.example.messages.databinding.MessageBinding
import com.brianguertin.example.messages.data.Message
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.circleCropTransform

class MessageViewHolder(
        private val binding: MessageBinding,
        private val listener: MessageListener
) : RecyclerView.ViewHolder(binding.root) {

    private val sideMargin = itemView.resources.getDimensionPixelOffset(R.dimen.message_margin)
    private val attachmentsAdapter = AttachmentsAdapter(listener)

    init {
        binding.attachments.isFocusableInTouchMode = false
        binding.attachments.layoutManager = object : LinearLayoutManager(binding.root.context) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.attachments.adapter = attachmentsAdapter
    }

    fun setMessage(message: Message) {
        val bubbleParams = binding.bubble.layoutParams as LinearLayout.LayoutParams
        if (message.author.id == 1L) {
            binding.avatar.visibility = View.GONE
            binding.name.setText(R.string.me)
            binding.name.gravity = Gravity.END
            binding.layout.gravity = Gravity.END
            binding.bubble.setBackgroundResource(R.drawable.message_bubble_me)
            bubbleParams.marginStart = sideMargin
            bubbleParams.marginEnd = 0
            Glide.with(binding.avatar.context)
                    .clear(binding.avatar)
        } else {
            binding.avatar.visibility = View.VISIBLE
            binding.name.text = message.author.name
            binding.name.gravity = Gravity.START
            binding.layout.gravity = Gravity.START
            binding.bubble.setBackgroundResource(R.drawable.message_bubble_other)
            bubbleParams.marginStart = 0
            bubbleParams.marginEnd = sideMargin
            Glide.with(binding.avatar.context)
                    .load(message.author.avatarUrl)
                    .apply(circleCropTransform())
                    .into(binding.avatar)
        }
        binding.bubble.layoutParams = bubbleParams
        binding.message.text = message.body
        attachmentsAdapter.setItems(message.attachments)
        binding.attachments.visibility = if (attachmentsAdapter.itemCount > 0) View.VISIBLE else View.GONE
        binding.bubble.setOnLongClickListener {
            listener.onLongClick(message)
            true
        }
    }
}