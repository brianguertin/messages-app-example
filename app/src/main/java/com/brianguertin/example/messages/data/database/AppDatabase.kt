package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context


@Database(entities = [MessageEntity::class, UserEntity::class, AttachmentEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val userDao: UserDao
    abstract val messageDao: MessageDao
    abstract val attachmentDao: AttachmentDao

    companion object {
        private var instance: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            synchronized(this) {
                return instance ?: Room
                        .databaseBuilder(
                                context.applicationContext,
                                AppDatabase::class.java,
                                "messages-database")
                        .build()
                        .also { instance = it }
            }
        }
    }
}
