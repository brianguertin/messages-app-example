package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface AttachmentDao {
    @Insert
    fun insert(repo: AttachmentEntity)

    @Query("DELETE FROM attachments WHERE id = :attachmentId")
    fun delete(attachmentId: String)

    @Query("SELECT * FROM attachments where messageId = :messageId")
    fun getForMessage(messageId: Long): List<AttachmentEntity>
}
