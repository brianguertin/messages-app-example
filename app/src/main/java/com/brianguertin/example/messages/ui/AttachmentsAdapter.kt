package com.brianguertin.example.messages.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.brianguertin.example.messages.databinding.AttachmentBinding
import com.brianguertin.example.messages.data.Attachment

class AttachmentsAdapter(private val listener: MessageListener) : RecyclerView.Adapter<AttachmentViewHolder>() {
    private var items: List<Attachment> = emptyList()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttachmentViewHolder {
        return AttachmentViewHolder(AttachmentBinding.inflate(
                LayoutInflater.from(parent.context), parent, false), listener)
    }

    override fun onBindViewHolder(holder: AttachmentViewHolder, position: Int) {
        holder.setAttachment(items[position])
    }

    fun setItems(items: List<Attachment>) {
        this.items = items
        notifyDataSetChanged()
    }
}