package com.brianguertin.example.messages.ui

import android.support.v7.widget.RecyclerView
import com.brianguertin.example.messages.databinding.AttachmentBinding
import com.brianguertin.example.messages.data.Attachment
import com.bumptech.glide.Glide

class AttachmentViewHolder(private val binding: AttachmentBinding,
                           private val listener: MessageListener
) : RecyclerView.ViewHolder(binding.root) {

    fun setAttachment(attachment: Attachment) {
        binding.title.text = attachment.title
        Glide.with(binding.image.context).load(attachment.thumbnailUrl).into(binding.image)
        itemView.setOnLongClickListener {
            listener.onLongClick(attachment)
            true
        }
    }
}