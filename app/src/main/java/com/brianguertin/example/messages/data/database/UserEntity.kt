package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity(
        @PrimaryKey
        var id: Long,
        var name: String,
        var avatarId: String
)