package com.brianguertin.example.messages.ui

import com.brianguertin.example.messages.data.Attachment
import com.brianguertin.example.messages.data.Message

interface MessageListener {
    fun onLongClick(message: Message)
    fun onLongClick(attachment: Attachment)
}