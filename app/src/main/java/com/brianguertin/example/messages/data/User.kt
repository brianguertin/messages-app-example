package com.brianguertin.example.messages.data

class User(
        val id: Long,
        val name: String,
        val avatarUrl: String)