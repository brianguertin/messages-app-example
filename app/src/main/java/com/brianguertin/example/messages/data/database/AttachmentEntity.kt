package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "attachments", foreignKeys = [
    ForeignKey(parentColumns = ["id"],
            childColumns = ["messageId"],
            entity = MessageEntity::class,
            onDelete = ForeignKey.CASCADE)
])
data class AttachmentEntity(
        @PrimaryKey
        var id: String,
        @ColumnInfo(index = true)
        var messageId: Long,
        var title: String,
        var url: String,
        var thumbnailUrl: String
)