package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface UserDao {
    @Insert
    fun insert(repo: UserEntity)

    @Query("SELECT * FROM users where id = :userId")
    fun getById(userId: Long): UserEntity
}
