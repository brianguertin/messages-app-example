package com.brianguertin.example.messages.data

data class Message(val id: Long,
                   val author: User,
                   val body: String,
                   val attachments: List<Attachment>)