package com.brianguertin.example.messages.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.brianguertin.example.messages.databinding.MessageBinding
import com.brianguertin.example.messages.data.Message

class MessagesAdapter(private val listener: MessageListener) : RecyclerView.Adapter<MessageViewHolder>() {
    private var items: List<Message> = emptyList()

    init {
        setHasStableIds(true)
    }

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder(MessageBinding.inflate(
                LayoutInflater.from(parent.context), parent, false), listener)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.setMessage(items.get(position))
    }

    override fun getItemId(position: Int): Long {
        return items[position].id
    }

    fun setItems(items: List<Message>) {
        this.items = items
        notifyDataSetChanged()
    }

}