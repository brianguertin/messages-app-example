package com.brianguertin.example.messages.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.brianguertin.example.messages.data.MessagesRepository

class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return when (modelClass) {
            MessagesViewModel::class.java ->
                MessagesViewModel(MessagesRepository(context))
            else -> throw IllegalArgumentException()
        } as T
    }

    companion object {
        inline fun <reified T : ViewModel> AppCompatActivity.getViewModelInstance(): T {
            return ViewModelProviders.of(this, ViewModelFactory(applicationContext))
                    .get(T::class.java)
        }
    }
}