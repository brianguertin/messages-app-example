package com.brianguertin.example.messages.data.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "messages", foreignKeys = [
    (ForeignKey(parentColumns = ["id"], childColumns = ["userId"], entity = UserEntity::class))])
data class MessageEntity(
        @PrimaryKey
        var id: Long,
        @ColumnInfo(index = true)
        var userId: Long,
        var content: String
)