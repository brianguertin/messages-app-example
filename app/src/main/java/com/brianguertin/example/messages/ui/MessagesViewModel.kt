package com.brianguertin.example.messages.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.brianguertin.example.messages.data.Attachment
import com.brianguertin.example.messages.data.Message
import com.brianguertin.example.messages.data.MessagesRepository
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

class MessagesViewModel(
        private val messagesRepository: MessagesRepository
) : ViewModel() {

    val messages: MutableLiveData<List<Message>> = MutableLiveData()

    private val job = Job()
    private var lastMessageId: Long? = null
    private var messageListBuilder = mutableListOf<Message>()
    private var loading = false

    init {
        if (lastMessageId == null) {
            loadNextPage()
        }
    }

    fun onScroll(lastVisibleItemIndex: Int) {
        if (lastVisibleItemIndex >= messageListBuilder.size - PAGE_LOAD_DISTANCE) {
            loadNextPage()
        }
    }

    fun onConfirmDeleteMessage(message: Message) {
        messageListBuilder.removeAll {
            it.id == message.id
        }
        messages.value = messageListBuilder.toList()
        launch(job + UI) {
            messagesRepository.delete(message)
        }
    }

    fun onConfirmDeleteAttachment(attachment: Attachment) {
        val index = messageListBuilder.indexOfFirst {
            it.attachments.find { it.id == attachment.id } != null
        }
        val oldMessage = messageListBuilder[index]
        messageListBuilder[index] = Message(
                id = oldMessage.id,
                body = oldMessage.body,
                author = oldMessage.author,
                attachments = oldMessage.attachments.filter {
                    it.id != attachment.id
                })
        messages.value = messageListBuilder.toList()
        launch(job + UI) {
            messagesRepository.delete(attachment)
        }
    }

    fun onDestroy() {
        job.cancel()
    }

    private fun loadNextPage() {
        if (loading) {
            return // Only load one page at a time
        }
        loading = true
        launch(job + UI) {
            messageListBuilder.addAll(messagesRepository.getPage(lastMessageId))
            lastMessageId = messageListBuilder.last().id
            messages.value = messageListBuilder.toList()
            loading = false
        }
    }

    companion object {
        /**
         * How many items from the last item we should start loading the next page
         */
        private const val PAGE_LOAD_DISTANCE = 1
    }
}
