package com.brianguertin.example.messages.data.importer

import android.content.res.AssetManager
import com.brianguertin.example.messages.data.database.AppDatabase
import com.brianguertin.example.messages.data.database.AttachmentEntity
import com.brianguertin.example.messages.data.database.MessageEntity
import com.brianguertin.example.messages.data.database.UserEntity
import com.google.gson.Gson

object DataFileImporter {
    fun read(assets: AssetManager): DataFile {
        assets.open("data.json").reader().use {
            return Gson().fromJson(it, DataFile::class.java)
        }
    }

    fun import(file: DataFile, database: AppDatabase) {
        for (user in file.users) {
            database.userDao.insert(UserEntity(
                    user.id,
                    user.name,
                    user.avatarId
            ))
        }
        for (message in file.messages) {
            database.messageDao.insert(MessageEntity(
                    message.id,
                    message.userId,
                    message.content
            ))
            message.attachments?.forEach { attachment ->
                database.attachmentDao.insert(AttachmentEntity(
                        attachment.id,
                        message.id,
                        attachment.title,
                        attachment.url,
                        attachment.thumbnailUrl
                ))
            }
        }
    }
}