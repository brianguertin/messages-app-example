package com.brianguertin.example.messages.data

import android.annotation.SuppressLint
import android.content.Context
import com.brianguertin.example.messages.data.database.AppDatabase
import com.brianguertin.example.messages.data.importer.DataFileImporter
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.withContext

class MessagesRepository(context: Context) {
    private val assets = context.assets
    private val database = AppDatabase.getInstance(context)
    private val prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE)

    suspend fun getPage(afterMessageId: Long?): List<Message> = withContext(CommonPool) {
        importDataIfNeeded()
        database.messageDao.list(afterMessageId ?: -1L, PAGE_SIZE).map { message ->
            val user = database.userDao.getById(message.userId).let {
                User(it.id, it.name, it.avatarId)
            }
            val attachments = database.attachmentDao.getForMessage(message.id).map {
                Attachment(it.id, it.title, it.thumbnailUrl)
            }
            Message(
                    id = message.id,
                    body = message.content,
                    author = user,
                    attachments = attachments)
        }
    }

    suspend fun delete(message: Message) = withContext(CommonPool) {
        database.messageDao.delete(message.id)
    }

    suspend fun delete(attachment: Attachment) = withContext(CommonPool) {
        database.attachmentDao.delete(attachment.id)
    }

    @SuppressLint("ApplySharedPref")
    private fun importDataIfNeeded() = synchronized(MessagesRepository::class) {
        if (!prefs.getBoolean(PREF_KEY_HAS_IMPORTED, false)) {
            database.clearAllTables()
            DataFileImporter.import(DataFileImporter.read(assets), database)
            prefs.edit().putBoolean(PREF_KEY_HAS_IMPORTED, true).commit()
        }
    }

    companion object {
        const val PAGE_SIZE = 20
        const val PREF_KEY_HAS_IMPORTED = "hasImported"
    }
}