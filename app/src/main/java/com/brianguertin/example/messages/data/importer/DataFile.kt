package com.brianguertin.example.messages.data.importer

data class DataFile(
        var messages: List<Message>,
        var users: List<User>
) {
    companion object {
        data class Attachment(
                var id: String,
                var title: String,
                var url: String,
                var thumbnailUrl: String
        )

        data class User(
                var id: Long,
                var name: String,
                var avatarId: String
        )

        data class Message(
                var id: Long,
                var userId: Long,
                var content: String,
                var attachments: List<Attachment>?
        )
    }
}

